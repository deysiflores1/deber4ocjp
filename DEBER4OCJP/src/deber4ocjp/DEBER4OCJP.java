/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deber4ocjp;

/**
 *
 * @author Jorge
 */
public class DEBER4OCJP {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Intancia de las clases:    "); 
        //Instancia de las diferentes clases
        Object objeto= new Object();
        //System.out.println(objeto.toString());
        Fruit fruit=new Fruit();
        System.out.println(fruit.toString());
        Apple apple=new Apple();
        System.out.println(apple.toString());
        Citrux citrus=new Citrux();
        System.out.println(citrus.toString());
        Orange orange= new Orange();
        System.out.println(orange.toString()); 
        
        Squeezable squeezable= new Squeezable() {};
        
       // System.out.println(squeezable.toString());
        System.out.println("Uso de casting y exception:   ");
        // A ClassCastException se lanza si falla el cast, si no se lanza ninguna excepción, el casting se ha realizado correctamente
        // Para cada intento de cast, imprimir un mensaje que indica si el casting tuvo éxito.
        
        //Casting y exception para Objeto
        try {
            
            objeto=(Fruit)fruit;
            System.out.print(objeto.toString()+"  ");
            System.out.print("OK    ");
            
        } catch (Exception e) {
            
            System.out.print("   No");
        }
        //Casting y exception para fruit
        
         try {
             
            fruit=(Apple)apple;
           System.out.print(fruit.toString()+"   ");
           
        } catch (Exception e) {
            
            System.out.print("  No");
             
        }
         
         //Casting y exception para apple
          try {
              
            apple=(Apple)(Squeezable)squeezable;
           System.out.print(apple.toString());
             System.out.print("OK    ");          
           
        } catch (Exception e) {
            
            System.out.print("  No");
            System.out.print("      ");
        }
          
          //Casting y exception para citrus
         try {
            citrus=(Citrux)citrus;
            System.out.print(citrus.toString()+"     ");
             System.out.print("OK    ");
            
            
        } catch (Exception e) {
            
            System.out.print("  No      ");
             System.out.println("     ");
        }
         
         //Casting y exception para orange
          try {
            orange=(Orange)orange;
           System.out.print(orange.toString());
            System.out.print("OK    ");
           
        } catch (Exception e) {
            
            System.out.print("   No");
        }
          System.out.println("");


    }
    
}
